<<<<<<< HEAD:problem-2/service.bal

import ballerina/graphql;




# Description of fields in the graphQL schema
#
# + regionCode - region code which is used as primary key
# + region - used to identify the region
# + deaths - Field for deaths  
# + confirmed_cases - Field for confirmed cases  
# + recoveries - Field for recoveries
# + tested - Field for tested
public type CovidEntry record {|
    readonly string regionCode;
    string region;
    decimal deaths?;
    decimal confirmed_cases?;
    decimal recoveries?;
    decimal tested?;
    
|};

public type CreatedCovidEntries record {|
   *http:Created;
   CovidEntry[] body;
|};

table<CovidEntry> key(regionCode) covidEntriesTable = table [
    {regionCode: "NA-KH", region: "Khomas",confirmed_cases: 159303, deaths: 7386, recoveries: 146084, tested: 5833},
    {regionCode: "NA-ER", region:  "Erongo",confirmed_cases: 598536, deaths: 15243, recoveries: 568637, tested: 14656},
    {regionCode: "NA-ON", region:  "Oshana",confirmed_cases: 69808350, deaths: 880976, recoveries: 43892277, tested: 25035097}
];

# Service to call the stats
public distinct service class CovidData {
    
    private final readonly & CovidEntry entryRecord;

    function init(CovidEntry entryRecord) {
        self.entryRecord = entryRecord.cloneReadOnly();
    }

    resource function get regionCode() returns string {
        return self.entryRecord.regionCode;
    }

    resource function get region() returns string {
        return self.entryRecord.region;
    }

    resource function get confirmed_cases() returns decimal? {
        if self.entryRecord.confirmed_cases is decimal {
            return self.entryRecord.confirmed_cases / 1000;
        }
        return;
    }

    resource function get deaths() returns decimal? {
        if self.entryRecord.deaths is decimal {
            return self.entryRecord.deaths / 1000;
        }
        return;
    }

    resource function get recoveries() returns decimal? {
        if self.entryRecord.recoveries is decimal {
            return self.entryRecord.recoveries / 1000;
        }
        return;
    }

    resource function get tested() returns decimal? {
        if self.entryRecord.tested is decimal {
            return self.entryRecord.tested / 1000;
        }
        return;
    }
}



service /covid19 on new graphql:Listener(9000) {
    resource function get all() returns CovidData[] {
        CovidEntry[] covidEntries = covidEntriesTable.toArray().cloneReadOnly();
        return covidEntries.map(entry => new CovidData(entry));
    }

    resource function get filter(string regionCode) returns CovidData? {
        CovidEntry? covidEntry = covidEntriesTable[regionCode];
        if covidEntry is CovidEntry {
            return new (covidEntry);
        }
        return;
    }

    remote function add(CovidEntry entry) returns CovidData {
        covidEntriesTable.add(entry);
        return new CovidData(entry);
    }
    
}






 
=======
import ballerina/http;
#remember to change int to something that cannot be overwritten!
public type StudentRecord record {
    readonly string student_num;
    string name;
    string course;
    decimal mark;
    
};

# Description
# keeps track of created students
# 
public type CreatedStudents record {
    *http:Created;
    StudentRecord[] body;
};

# Invalid Student Error, used when student nums conflict with existing ones
public type InvalidStudentNumError record {
    *http:Conflict;
    ErrorMsg body;

};

public type ErrorMsg record {
    string errmsg;
};

# Students Table 
public final table<StudentRecord> key(student_num)  StudentTable = table [
    {student_num: "220090210" , name: "Adrian", course:"Computer Science" , mark: 87}
    
];
#service function to retrieve data from students table
# use resource function to get requests
service /StudentRecord/students on new http:Listener(9000) {
    resource function get students() returns StudentRecord[]  {
        return StudentTable.toArray();
    }
    # may have to change the string to an int to accomodate it in the arrays length?
   
 resource function post countries(@http:Payload StudentRecord[] StudentRecords)
                                    returns CreatedStudents|InvalidStudentNumError {
    
    string [] conflictingStudents = from StudentRecord studentRecord in StudentRecords
        where StudentTable.hasKey(studentRecord.student_num)
        select studentRecord.student_num;

    if conflictingStudents.length() > 0 {
        return <InvalidStudentNumError>{
            body: {
                errmsg: string:'join(" ", "Student Numbers:", ...conflictingStudents)
            }
        };
    } else {
        StudentRecords.forEach(studentRecord => StudentTable.add(student_num));
        return <CreatedStudents>{body: StudentRecords};
    }
    resource function get students/[string student_num]() returns StudentRecord|InvalidStudentNumError {
    StudentRecord? studentRecord = StudentTable[student_num];
    if studentRecord is () {
        return {
            body: {
                errmsg: string `Invalid Student Number: ${student_num}`
            }
        };
    }
    return studentRecord;
}
}
>>>>>>> 2ecaf76797a74c5c14b04383153e70ef05bd8d0b:problem-3/student_record/service.bal
